package by.itstep.collections.manager;

import by.itstep.collections.manager.entity.Collection;
import by.itstep.collections.manager.repository.CollectionRepository;
import by.itstep.collections.manager.repository.impl.CollectionItemRepositoryImpl;
import by.itstep.collections.manager.repository.impl.CollectionRepositoryImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class ApplicationTests {

    private CollectionRepository repository = new CollectionRepositoryImpl();

    @BeforeEach
    void setUp(){
        repository.deleteAll();
    }

	@Test
	void testCreate() {
	    //given
        Collection collection = Collection.builder()
                .name("my-name")
                .description("my-description")
                .imageUrl("my-image")
                .title("my-title")
                .build();

        //when
        Collection saved = repository.create(collection);

        //then
        Assertions.assertNotNull(saved.getId());
	}

	@Test
	void testfindAll() {
        //given 2 coll
        Collection collection1 = Collection.builder()
                .name("my-name")
                .description("my-description")
                .imageUrl("my-image")
                .title("my-title")
                .build();

        Collection collection2 = Collection.builder()
                .name("my-name2")
                .description("my-description2")
                .imageUrl("my-image2")
                .title("my-title2")
                .build();

        Collection saved1 = repository.create(collection1);
        Collection saved2 = repository.create(collection2);

        //when findAll

        List<Collection> found = repository.findAll();


        //then проверить что пришло 2 коллекции
        Assertions.assertEquals(2,found.size());
	}

	@Test
	void test3() {
	}

	@Test
	void test4() {
	}

	@Test
	void test5() {
	}

}
