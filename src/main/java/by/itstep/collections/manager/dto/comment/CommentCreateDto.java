package by.itstep.collections.manager.dto.comment;

import by.itstep.collections.manager.entity.Collection;
import by.itstep.collections.manager.entity.User;
import lombok.Data;

import java.sql.Date;

@Data
public class CommentCreateDto {

    private String message;
    private Long userId;
    private Long collectionId;
}
