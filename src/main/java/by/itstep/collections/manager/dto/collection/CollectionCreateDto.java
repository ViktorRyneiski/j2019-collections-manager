package by.itstep.collections.manager.dto.collection;

import by.itstep.collections.manager.entity.Tag;
import lombok.Data;

import java.util.List;
@Data
public class CollectionCreateDto {

    private String name;
    private String title;
    private String description;
    private String imageUrl;
    private Long userId;

}
