package by.itstep.collections.manager.dto.user;

import by.itstep.collections.manager.entity.enums.Role;
import lombok.Data;

import javax.persistence.Column;

@Data
public class UserPreviewDto {

    private Long id;
    private String firstName;
    private String lastName;
    private String email;
}
