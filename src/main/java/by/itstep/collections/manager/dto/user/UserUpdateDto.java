package by.itstep.collections.manager.dto.user;

import by.itstep.collections.manager.entity.enums.Role;
import lombok.Data;

@Data
public class UserUpdateDto {

    private Long id;
    private String firstName;
    private String lastName;
}
