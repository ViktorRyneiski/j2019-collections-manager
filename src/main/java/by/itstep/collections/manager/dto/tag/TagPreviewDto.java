package by.itstep.collections.manager.dto.tag;

import lombok.Data;

@Data
public class TagPreviewDto {

    private Long id;
    private String name;
}
