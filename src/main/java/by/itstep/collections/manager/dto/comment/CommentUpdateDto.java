package by.itstep.collections.manager.dto.comment;

import by.itstep.collections.manager.entity.Collection;
import by.itstep.collections.manager.entity.User;
import lombok.Data;

import java.sql.Date;

@Data
public class CommentUpdateDto {
    private Long id;
    private String message;
}
