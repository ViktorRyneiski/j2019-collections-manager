package by.itstep.collections.manager.controller;

import by.itstep.collections.manager.dto.collection.CollectionPreviewDto;
import by.itstep.collections.manager.dto.user.UserFullDto;
import by.itstep.collections.manager.entity.Collection;
import by.itstep.collections.manager.service.CollectionService;
import by.itstep.collections.manager.service.UserService;
import by.itstep.collections.manager.service.impl.CollectionServiceImpl;
import by.itstep.collections.manager.service.impl.UserServiceImpl;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;

@Controller
public class CollectionController {
/*
API:
1. Получить главную страницу
2. Получить страницу по ИД коллекции
3. Получить страницу с формой создания коллекции
4. Дать возможность создать коллекцию из форм
 */

    private final CollectionService collectionService = new CollectionServiceImpl();
    private final UserService userService = new UserServiceImpl();


    @RequestMapping(method = RequestMethod.GET, value = "/index")
    public String openMainPage(Model model) {
        List<CollectionPreviewDto> found = collectionService.findAll();
        model.addAttribute("value","Hello from thymeleaf");
        model.addAttribute("all_collections", generateAll());
        return "index";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/profile/{id}")
    public String openProfile(Model model, @PathVariable Long id) {
        UserFullDto found = userService.findById(id);
        model.addAttribute("user",found);

        return "profile";
    }

    private List<CollectionPreviewDto> generateAll(){
        List<CollectionPreviewDto> generated = new ArrayList<>();
        generated.add(new  CollectionPreviewDto(1L,"super-coll","title-1","img-url1","Bob",null));
        generated.add(new  CollectionPreviewDto(2L,"super-coll","title-2","img-url1","Bob",null));
        generated.add(new  CollectionPreviewDto(3L,"super-coll","title-3","img-url1","Bob",null));
        generated.add(new  CollectionPreviewDto(4L,"super-coll","title-4","img-url1","Bob",null));
        generated.add(new  CollectionPreviewDto(5L,"super-coll","title-5","img-url1","Bob",null));

        return generated;
    }

//    @RequestMapping(method = RequestMethod.GET,value = "/path/{var}/params")
//    public String getMainPage(@PathVariable Long var, @RequestParam String a,
//                              @RequestParam String b){
//
//        System.out.println("VAR: " + var);
//        System.out.println("a: " + a);
//        System.out.println("b: " + b);
//
//        return "index";
//    }
}
