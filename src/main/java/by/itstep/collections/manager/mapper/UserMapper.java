package by.itstep.collections.manager.mapper;

import by.itstep.collections.manager.dto.user.UserCreateDto;
import by.itstep.collections.manager.dto.user.UserFullDto;
import by.itstep.collections.manager.dto.user.UserPreviewDto;
import by.itstep.collections.manager.dto.user.UserUpdateDto;
import by.itstep.collections.manager.entity.User;

import java.util.ArrayList;
import java.util.List;

public class UserMapper {

    public List<UserPreviewDto> mapToDtoList(final List<User> entities) {
        List<UserPreviewDto> dtos = new ArrayList<>();
        for (User entity:entities) {
            UserPreviewDto dto = new UserPreviewDto();
            dto.setEmail(entity.getEmail());
            dto.setFirstName(entity.getFirstName());
            dto.setLastName(entity.getLastName());
            dto.setId(entity.getId());

            dtos.add(dto);
        }
        return dtos;
    }

    public UserFullDto mapToDto(User entity){
        UserFullDto dto = new UserFullDto();
        dto.setId(entity.getId());
        dto.setEmail(entity.getEmail());
        dto.setFirstName(entity.getFirstName());
        dto.setLastName(entity.getLastName());
        dto.setRole(entity.getRole());
        dto.setCollections(entity.getCollections());
        dto.setComments(entity.getComments());
        return dto;
    }

    public User mapToEntity(UserCreateDto userCreateDto){

        User entity = new User();

        entity.setEmail(userCreateDto.getEmail());
        entity.setFirstName(userCreateDto.getFirstName());
        entity.setLastName(userCreateDto.getLastName());

        return entity;
    }

    public User mapToEntity(UserUpdateDto userUpdateDto){

        User entity = new User();

        entity.setId(userUpdateDto.getId());
        entity.setFirstName(userUpdateDto.getFirstName());
        entity.setLastName(userUpdateDto.getLastName());

        return entity;
    }

}
