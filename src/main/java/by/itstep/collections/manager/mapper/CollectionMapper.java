package by.itstep.collections.manager.mapper;

import by.itstep.collections.manager.dto.collection.CollectionCreateDto;
import by.itstep.collections.manager.dto.collection.CollectionFullDto;
import by.itstep.collections.manager.dto.collection.CollectionPreviewDto;
import by.itstep.collections.manager.dto.collection.CollectionUpdateDto;
import by.itstep.collections.manager.entity.Collection;
import by.itstep.collections.manager.entity.User;

import java.util.ArrayList;
import java.util.List;

public class CollectionMapper {

    public List<CollectionPreviewDto> mapToDtoList(List<Collection> entities) {
        List<CollectionPreviewDto> dtos = new ArrayList<>();

        for (Collection entity : entities) {
            CollectionPreviewDto dto = new CollectionPreviewDto();
            dto.setId(entity.getId());
            dto.setImageUrl(entity.getImageUrl());
            dto.setName(entity.getName());
            dto.setTags(entity.getTags());
            dto.setTitle(entity.getTitle());
            dto.setUserName(entity.getUser().getFirstName() + " " + entity.getUser().getLastName());

            dtos.add(dto);
        }

        return dtos;
    }

    public Collection mapToEntity(CollectionCreateDto createDto, User user) {
        Collection collection = new Collection();
        collection.setDescription(createDto.getDescription());
        collection.setName(createDto.getName());
        collection.setTitle(createDto.getTitle());
        collection.setImageUrl(createDto.getImageUrl());
        collection.setUser(user);

        return collection;
    }

    public Collection mapToEntity(CollectionUpdateDto updateDto) {
        Collection collection = new Collection();
        collection.setId(updateDto.getId());
        collection.setDescription(updateDto.getDescription());
        collection.setName(updateDto.getName());
        collection.setTitle(updateDto.getTitle());

        return collection;
    }

    public CollectionFullDto mapToDto(final Collection entity) {
        CollectionFullDto dto = new CollectionFullDto();

        dto.setId(entity.getId());
        dto.setDescription(entity.getDescription());
        dto.setImageUrl(entity.getImageUrl());
        dto.setName(entity.getName());
        dto.setTitle(entity.getTitle());
        dto.setUser(entity.getUser());

        return dto;
    }

}
