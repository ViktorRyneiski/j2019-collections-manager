package by.itstep.collections.manager.mapper;

import by.itstep.collections.manager.dto.collection.CollectionCreateDto;
import by.itstep.collections.manager.dto.collection.CollectionFullDto;
import by.itstep.collections.manager.dto.collection.CollectionPreviewDto;
import by.itstep.collections.manager.dto.collection.CollectionUpdateDto;
import by.itstep.collections.manager.dto.collectionItem.CollectionItemCreateDto;
import by.itstep.collections.manager.dto.collectionItem.CollectionItemFullDto;
import by.itstep.collections.manager.dto.collectionItem.CollectionItemPreviewDto;
import by.itstep.collections.manager.dto.collectionItem.CollectionItemUpdateDto;
import by.itstep.collections.manager.entity.Collection;
import by.itstep.collections.manager.entity.CollectionItem;
import by.itstep.collections.manager.entity.User;

import java.util.ArrayList;
import java.util.List;

public class CollectionItemMapper {

    public List<CollectionItemPreviewDto> mapToDtoList(List<CollectionItem> entities) {
        List<CollectionItemPreviewDto> dtos = new ArrayList<>();

        for (CollectionItem entity : entities) {
            CollectionItemPreviewDto dto = new CollectionItemPreviewDto();
            dto.setId(entity.getId());
            dto.setName(entity.getName());

            dtos.add(dto);
        }

        return dtos;
    }

    public CollectionItem mapToEntity(CollectionItemCreateDto createDto, Collection collection) {
        CollectionItem collectionItem = new CollectionItem();
        collectionItem.setName(createDto.getName());
        collectionItem.setCollection(collection);

        return collectionItem;
    }

    public CollectionItem mapToEntity(CollectionItemUpdateDto updateDto) {
        CollectionItem collectionItem = new CollectionItem();
        collectionItem.setId(updateDto.getId());
        collectionItem.setName(updateDto.getName());

        return collectionItem;
    }

    public CollectionItemFullDto mapToDto(final CollectionItem entity) {
        CollectionItemFullDto dto = new CollectionItemFullDto();

        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setCollection(entity.getCollection());

        return dto;
    }
}
