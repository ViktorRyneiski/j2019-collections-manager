package by.itstep.collections.manager.mapper;

import by.itstep.collections.manager.dto.collection.CollectionUpdateDto;
import by.itstep.collections.manager.dto.comment.CommentCreateDto;
import by.itstep.collections.manager.dto.comment.CommentFullDto;
import by.itstep.collections.manager.dto.comment.CommentPreviewDto;
import by.itstep.collections.manager.dto.comment.CommentUpdateDto;
import by.itstep.collections.manager.entity.Collection;
import by.itstep.collections.manager.entity.Comment;
import by.itstep.collections.manager.entity.User;
import by.itstep.collections.manager.repository.CommentRepository;
import by.itstep.collections.manager.repository.impl.CommentRepositoryImpl;

import java.util.ArrayList;
import java.util.List;

public class CommentMapper {

    public List<CommentPreviewDto> mapToDtoList(List<Comment> entities){
        List<CommentPreviewDto> dtos = new ArrayList<>();

        for (Comment entity:entities) {
            CommentPreviewDto dto = new CommentPreviewDto();
            dto.setId(entity.getId());
            dto.setMessage(entity.getMessage());
            dtos.add(dto);
        }
        return dtos;
    }

    public Comment mapToEntity(CommentCreateDto createDto, Collection collection, User user){
        Comment entity = new Comment();
        entity.setMessage(createDto.getMessage());
        entity.setCollection(collection);
        entity.setUser(user);

        return entity;
    }

    public Comment mapToEntity(CommentUpdateDto updateDto){
        Comment entity = new Comment();
        entity.setId(updateDto.getId());
        entity.setMessage(updateDto.getMessage());

        return entity;
    }

    public CommentFullDto mapToDto(Comment entity){
        CommentFullDto dto = new CommentFullDto();
        dto.setId(entity.getId());
        dto.setCreatedAt(entity.getCreatedAt());
        dto.setMessage(entity.getMessage());
        dto.setCollection(entity.getCollection());
        dto.setUser(entity.getUser());

        return dto;
    }

}
