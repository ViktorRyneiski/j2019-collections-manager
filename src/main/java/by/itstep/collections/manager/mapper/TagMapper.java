package by.itstep.collections.manager.mapper;

import by.itstep.collections.manager.dto.tag.TagCreateDto;
import by.itstep.collections.manager.dto.tag.TagFullDto;
import by.itstep.collections.manager.dto.tag.TagPreviewDto;
import by.itstep.collections.manager.dto.tag.TagUpdateDto;
import by.itstep.collections.manager.entity.Tag;

import java.util.ArrayList;
import java.util.List;

public class TagMapper {

    public List<TagPreviewDto> mapToDtoList(List<Tag> entities){
        List<TagPreviewDto> dtos = new ArrayList<>();

        for (Tag entity:entities) {
            TagPreviewDto dto = new TagPreviewDto();
            dto.setId(entity.getId());
            dto.setName(entity.getName());

            dtos.add(dto);
        }
        return dtos;
    }

    public Tag mapToEntity(TagCreateDto createDto){

        Tag entity = new Tag();
        entity.setName(createDto.getName());

        return entity;
    }

    public Tag mapToEntity(TagUpdateDto updateDto){

        Tag entity = new Tag();
        entity.setId(updateDto.getId());
        entity.setName(updateDto.getName());

        return entity;
    }

    public TagFullDto mapToDto(Tag entity){

        TagFullDto dto = new TagFullDto();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setCollections(entity.getCollections());

        return dto;
    }
}
