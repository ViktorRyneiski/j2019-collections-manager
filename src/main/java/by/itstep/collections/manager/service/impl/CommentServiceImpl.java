package by.itstep.collections.manager.service.impl;

import by.itstep.collections.manager.dto.comment.CommentCreateDto;
import by.itstep.collections.manager.dto.comment.CommentFullDto;
import by.itstep.collections.manager.dto.comment.CommentPreviewDto;
import by.itstep.collections.manager.dto.comment.CommentUpdateDto;
import by.itstep.collections.manager.entity.Collection;
import by.itstep.collections.manager.entity.Comment;
import by.itstep.collections.manager.entity.User;
import by.itstep.collections.manager.mapper.CommentMapper;
import by.itstep.collections.manager.repository.CollectionRepository;
import by.itstep.collections.manager.repository.CommentRepository;
import by.itstep.collections.manager.repository.UserRepository;
import by.itstep.collections.manager.repository.impl.CollectionRepositoryImpl;
import by.itstep.collections.manager.repository.impl.CommentRepositoryImpl;
import by.itstep.collections.manager.repository.impl.UserRepositoryImpl;
import by.itstep.collections.manager.service.CommentService;

import java.util.List;

public class CommentServiceImpl implements CommentService {

    private final CommentRepository commentRepository = new CommentRepositoryImpl();
    private final CommentMapper commentMapper = new CommentMapper();

    private final CollectionRepository collectionRepository = new CollectionRepositoryImpl();

    private final UserRepository userRepository = new UserRepositoryImpl();

    @Override
    public List<CommentPreviewDto> findAll() {
        List<Comment> found = commentRepository.findAll();
        System.out.println("CommentServiceImpl -> found " + found.size() + " Comments");
        List<CommentPreviewDto> dtos = commentMapper.mapToDtoList(found);

        return dtos;
    }

    @Override
    public CommentFullDto findById(final Long id) {
        Comment found = commentRepository.findById(id);
        System.out.println("CommentServiceImpl -> found comment " + found);
        CommentFullDto dto = commentMapper.mapToDto(found);
        return dto;
    }

    @Override
    public CommentFullDto create(final CommentCreateDto createDto) {
        Collection collection = collectionRepository.findById(createDto.getCollectionId());
        User user = userRepository.findById(createDto.getUserId());

        Comment toSave = commentMapper.mapToEntity(createDto,collection,user);
        Comment created = commentRepository.create(toSave);

        CommentFullDto dto = commentMapper.mapToDto(created);
        System.out.println("CommentServiceImpl -> create comment " + created);
        return dto;
    }

    @Override
    public CommentFullDto update(final CommentUpdateDto updateDto) {
        Comment toUpdate = commentMapper.mapToEntity(updateDto);
        Comment existingEntity = commentRepository.findById(updateDto.getId());
        toUpdate.setUser(existingEntity.getUser());
        toUpdate.setCollection(existingEntity.getCollection());
        toUpdate.setCreatedAt(existingEntity.getCreatedAt());

        Comment updated = commentRepository.update(toUpdate);

        CommentFullDto dto = commentMapper.mapToDto(updated);
        System.out.println("CommentServiceImpl -> create comment " + updated);
        return dto;
    }

    @Override
    public void delete(final Long id) {
        commentRepository.delete(id);
        System.out.println("CommentServiceImpl -> comment with id: " + id + " was deleted");
    }
}
