package by.itstep.collections.manager.service.impl;

import by.itstep.collections.manager.dto.collectionItem.CollectionItemCreateDto;
import by.itstep.collections.manager.dto.collectionItem.CollectionItemFullDto;
import by.itstep.collections.manager.dto.collectionItem.CollectionItemPreviewDto;
import by.itstep.collections.manager.dto.collectionItem.CollectionItemUpdateDto;
import by.itstep.collections.manager.entity.Collection;
import by.itstep.collections.manager.entity.CollectionItem;
import by.itstep.collections.manager.mapper.CollectionItemMapper;
import by.itstep.collections.manager.repository.CollectionItemRepository;
import by.itstep.collections.manager.repository.CollectionRepository;
import by.itstep.collections.manager.repository.impl.CollectionItemRepositoryImpl;
import by.itstep.collections.manager.repository.impl.CollectionRepositoryImpl;
import by.itstep.collections.manager.service.CollectionItemService;

import java.util.List;

public class CollectionItemServiceImpl implements CollectionItemService {

    private final CollectionItemRepository itemRepository = new CollectionItemRepositoryImpl();
    private final CollectionRepository collectionRepository = new CollectionRepositoryImpl();
    private final CollectionItemMapper mapper = new CollectionItemMapper();

    @Override
    public List<CollectionItemPreviewDto> findAll() {
        List<CollectionItemPreviewDto> dtos = mapper.mapToDtoList(itemRepository.findAll());

        System.out.println("CollectionItemServiceImpl -> found " + dtos.size() + " collections");

        return dtos;
    }

    @Override
    public CollectionItemFullDto findById(final Long id) {
        CollectionItemFullDto dto = mapper.mapToDto(itemRepository.findById(id));
        System.out.println("CollectionItemServiceImpl -> found collection " + dto);
        return dto;
    }

    @Override
    public CollectionItemFullDto create(final CollectionItemCreateDto createDto) {
        Collection collection = collectionRepository.findById(createDto.getCollectionId());
        CollectionItem toSave = mapper.mapToEntity(createDto, collection);

        final CollectionItem created = itemRepository.create(toSave);

        CollectionItemFullDto createdDto = mapper.mapToDto(created);
        System.out.println("CollectionItemServiceImpl -> create collection " + created);
        return createdDto;
    }

    @Override
    public CollectionItemFullDto update(final CollectionItemUpdateDto updateDto) {
        CollectionItem toUpdate = mapper.mapToEntity(updateDto);
        CollectionItem existingEntity = itemRepository.findById(updateDto.getId());

        toUpdate.setCollection(existingEntity.getCollection());

        final CollectionItem updated = itemRepository.update(toUpdate);
        CollectionItemFullDto updatedDto = mapper.mapToDto(updated);

        System.out.println("CollectionServiceImpl -> update collection " + updated);
        return updatedDto;
    }

    @Override
    public void delete(final Long id) {

        itemRepository.delete(id);
        System.out.println("CollectionItemServiceImpl -> collectionItem with id: " + id + " was deleted");

    }
}
