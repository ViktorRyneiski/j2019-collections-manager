package by.itstep.collections.manager.service;

import by.itstep.collections.manager.dto.collection.CollectionCreateDto;
import by.itstep.collections.manager.dto.collection.CollectionFullDto;
import by.itstep.collections.manager.dto.collection.CollectionPreviewDto;
import by.itstep.collections.manager.dto.collection.CollectionUpdateDto;
import by.itstep.collections.manager.dto.comment.CommentCreateDto;
import by.itstep.collections.manager.dto.comment.CommentFullDto;
import by.itstep.collections.manager.dto.comment.CommentPreviewDto;
import by.itstep.collections.manager.dto.comment.CommentUpdateDto;

import java.util.List;

public interface CommentService {

    List<CommentPreviewDto> findAll();

    CommentFullDto findById(Long id);

    CommentFullDto create(CommentCreateDto createDto);

    CommentFullDto update(CommentUpdateDto updateDto);

    void delete(Long id);
}
