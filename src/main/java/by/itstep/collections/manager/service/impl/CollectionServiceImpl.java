package by.itstep.collections.manager.service.impl;

import by.itstep.collections.manager.dto.collection.CollectionCreateDto;
import by.itstep.collections.manager.dto.collection.CollectionFullDto;
import by.itstep.collections.manager.dto.collection.CollectionPreviewDto;
import by.itstep.collections.manager.dto.collection.CollectionUpdateDto;
import by.itstep.collections.manager.entity.Collection;
import by.itstep.collections.manager.entity.User;
import by.itstep.collections.manager.exception.InvalidDtoException;
import by.itstep.collections.manager.exception.MissedUpdateIdException;
import by.itstep.collections.manager.mapper.CollectionMapper;
import by.itstep.collections.manager.repository.CollectionRepository;
import by.itstep.collections.manager.repository.UserRepository;
import by.itstep.collections.manager.repository.impl.CollectionRepositoryImpl;
import by.itstep.collections.manager.repository.impl.UserRepositoryImpl;
import by.itstep.collections.manager.service.CollectionService;

import java.util.List;

public class CollectionServiceImpl implements CollectionService {

    private final CollectionRepository collectionRepository = new CollectionRepositoryImpl();
    private final UserRepository userRepository = new UserRepositoryImpl();
    private final CollectionMapper mapper = new CollectionMapper();

    @Override
    public List<CollectionPreviewDto> findAll() {
        List<Collection> found = collectionRepository.findAll();
        System.out.println("CollectionServiceImpl -> found " + found.size() + " collections");
        List<CollectionPreviewDto> converted = mapper.mapToDtoList(found);
        return converted;
    }

    @Override
    public CollectionFullDto findById(final Long id) {
        final Collection found = collectionRepository.findById(id);
        CollectionFullDto dto = mapper.mapToDto(found);
        System.out.println("CollectionServiceImpl -> found collection " + found);
        return dto;
    }

    @Override
    public CollectionFullDto create(final CollectionCreateDto createDto) throws InvalidDtoException {

        validateCreateDto(createDto);
        User user = userRepository.findById(createDto.getUserId());
        Collection toSave = mapper.mapToEntity(createDto,user);

        final Collection created = collectionRepository.create(toSave);

        CollectionFullDto createdDto = mapper.mapToDto(created);
        System.out.println("CollectionServiceImpl -> create collection " + created);
        return createdDto;
    }

    @Override
    public CollectionFullDto update(final CollectionUpdateDto updateDto) throws MissedUpdateIdException {

        validateUpdateDto(updateDto);
        Collection toUpdate = mapper.mapToEntity(updateDto);
        Collection existingEntity = collectionRepository.findById(updateDto.getId());

        toUpdate.setUser(existingEntity.getUser());
        toUpdate.setTags(existingEntity.getTags());
        toUpdate.setImageUrl(existingEntity.getImageUrl());
        toUpdate.setComments(existingEntity.getComments());
        toUpdate.setItems(existingEntity.getItems());

        final Collection updated = collectionRepository.update(toUpdate);

        CollectionFullDto updatedDto = mapper.mapToDto(updated);
        System.out.println("CollectionServiceImpl -> update collection " + updated);
        return updatedDto;
    }

    @Override
    public void delete(final Long id) {
        collectionRepository.delete(id);
        System.out.println("CollectionServiceImpl -> collection with id: " + id + " was deleted");
    }

    private void validateCreateDto(CollectionCreateDto createDto) throws InvalidDtoException {
        if (createDto.getUserId() == null || createDto.getDescription() == null ||
                createDto.getName() == null || createDto.getTitle() == null){
            throw new InvalidDtoException("one or more fields is null");
        }
        if (createDto.getDescription().length() < 15){
            throw new InvalidDtoException("description min length is 15");
        }

    }

    private void validateUpdateDto(CollectionUpdateDto updateDto) throws MissedUpdateIdException {
        if (updateDto.getId() == null){
            throw new MissedUpdateIdException("Collection id is not specified");
        }

    }
}
