package by.itstep.collections.manager.service;

import by.itstep.collections.manager.dto.collection.CollectionCreateDto;
import by.itstep.collections.manager.dto.collection.CollectionFullDto;
import by.itstep.collections.manager.dto.collection.CollectionPreviewDto;
import by.itstep.collections.manager.dto.collection.CollectionUpdateDto;
import by.itstep.collections.manager.dto.collectionItem.CollectionItemCreateDto;
import by.itstep.collections.manager.dto.collectionItem.CollectionItemFullDto;
import by.itstep.collections.manager.dto.collectionItem.CollectionItemPreviewDto;
import by.itstep.collections.manager.dto.collectionItem.CollectionItemUpdateDto;

import java.util.List;

public interface CollectionItemService {

    List<CollectionItemPreviewDto> findAll();

    CollectionItemFullDto findById(Long id);

    CollectionItemFullDto create(CollectionItemCreateDto createDto);

    CollectionItemFullDto update(CollectionItemUpdateDto collection);

    void delete(Long id);
}
