package by.itstep.collections.manager.repository.impl;

import by.itstep.collections.manager.entity.Collection;
import by.itstep.collections.manager.repository.CollectionRepository;
import by.itstep.collections.manager.util.EntityManagerUtils;
import org.hibernate.Hibernate;

import java.util.List;
import javax.persistence.EntityManager;

public class CollectionRepositoryImpl implements CollectionRepository {

    @Override
    public List<Collection> findAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();

        List<Collection> foundCollections = em.createNativeQuery("SELECT * FROM collections",Collection.class)
                .getResultList();

        em.close();

        System.out.println("Found: " + foundCollections.size() + " collections");
        return foundCollections;
    }

    @Override
    public Collection findById(final Long id) {
        EntityManager em = EntityManagerUtils.getEntityManager();

        Collection foundCollection = em.find(Collection.class,id);


        Hibernate.initialize(foundCollection.getItems());

        em.close();
        System.out.println("Found collection: " + foundCollection);
        return foundCollection;
    }

    @Override
    public Collection create(final Collection collection) {
        EntityManager em = EntityManagerUtils.getEntityManager();

        em.getTransaction().begin();

        em.persist(collection);

        em.getTransaction().commit();

        em.close();
        System.out.println("Collection was created. id:" + collection.getId());
        return collection;
    }

    @Override
    public Collection update(final Collection collection) {
        EntityManager em = EntityManagerUtils.getEntityManager();

        em.getTransaction().begin();

        em.merge(collection);

        em.getTransaction().commit();

        em.close();
        System.out.println("Collection was updated. id:" + collection.getId());

        return collection;
    }

    @Override
    public void delete(final Long id) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        Collection toDelete = em.find(Collection.class,id);
        em.remove(toDelete);

        em.getTransaction().commit();
        em.close();
        System.out.println("Collection was deleted. id:" + toDelete.getId());

    }

    @Override
    public void deleteAll() {

        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.createNativeQuery("DELETE FROM collections").executeUpdate();

        em.getTransaction().commit();
        em.close();
    }
}
