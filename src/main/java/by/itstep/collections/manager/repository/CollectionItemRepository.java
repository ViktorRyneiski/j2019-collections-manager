package by.itstep.collections.manager.repository;

import by.itstep.collections.manager.entity.Collection;
import by.itstep.collections.manager.entity.CollectionItem;
import by.itstep.collections.manager.util.EntityManagerUtils;

import java.util.List;
import javax.persistence.EntityManager;

public interface CollectionItemRepository {

    List<CollectionItem> findAll();

    CollectionItem findById(Long id);

    CollectionItem create(CollectionItem collectionItem);

    CollectionItem update(CollectionItem collectionItem);

    void delete(Long id);

    void deleteAll();
}
