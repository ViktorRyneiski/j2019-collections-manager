package by.itstep.collections.manager.repository.impl;

import by.itstep.collections.manager.entity.Comment;
import by.itstep.collections.manager.entity.User;
import by.itstep.collections.manager.repository.UserRepository;
import by.itstep.collections.manager.util.EntityManagerUtils;

import java.util.List;
import javax.persistence.EntityManager;

public class UserRepositoryImpl implements UserRepository {

    @Override
    public List<User> findAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();

        List<User> foundUsers = em.createNativeQuery("SELECT * FROM users",User.class)
                .getResultList();

        em.close();

        System.out.println("Found: " + foundUsers.size() + " users");
        return foundUsers;
    }

    @Override
    public User findById(final Long id) {
        EntityManager em = EntityManagerUtils.getEntityManager();

        User foundUser = em.find(User.class,id);

        em.close();

        System.out.println("Found user: " + foundUser);
        return foundUser;
    }

    @Override
    public User create(final User user) {
        EntityManager em = EntityManagerUtils.getEntityManager();

        em.getTransaction().begin();

        em.persist(user);

        em.getTransaction().commit();

        em.close();
        System.out.println("User was created. id:" + user.getId());
        return user;
    }

    @Override
    public User update(final User user) {
        EntityManager em = EntityManagerUtils.getEntityManager();

        em.getTransaction().begin();

        em.persist(user);

        em.getTransaction().commit();

        em.close();
        System.out.println("User was updated. id:" + user.getId());
        return user;
    }

    @Override
    public void delete(final Long id) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        User toDelete = em.find(User.class,id);
        em.remove(toDelete);

        em.getTransaction().commit();
        em.close();
        System.out.println("User was deleted. id:" + toDelete.getId());

    }
}
