package by.itstep.collections.manager.repository;

import by.itstep.collections.manager.entity.Collection;

import java.util.List;

public interface CollectionRepository {

    List<Collection> findAll();

//    List<Collection> findByUserId();

    Collection findById(Long id);

    Collection create(Collection collection);

    Collection update(Collection collection);

    void delete(Long id);

    void deleteAll();
}
