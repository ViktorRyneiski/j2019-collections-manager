package by.itstep.collections.manager.repository.impl;

import by.itstep.collections.manager.entity.Tag;
import by.itstep.collections.manager.repository.TagRepository;
import by.itstep.collections.manager.util.EntityManagerUtils;

import java.util.List;
import javax.persistence.EntityManager;

public class TagRepositoryImpl implements TagRepository {

    @Override
    public List<Tag> findAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();

        List<Tag> foundTags = em.createNativeQuery("SELECT * FROM tags",Tag.class)
                .getResultList();

        em.close();

        System.out.println("Found: " + foundTags.size() + " tags");
        return foundTags;
    }

    @Override
    public Tag findById(final Long id) {
        EntityManager em = EntityManagerUtils.getEntityManager();

        Tag foundTag = em.find(Tag.class,id);

        em.close();

        System.out.println("Found tag: " + foundTag);
        return foundTag;
    }

    @Override
    public Tag create(final Tag tag) {
        EntityManager em = EntityManagerUtils.getEntityManager();

        em.getTransaction().begin();

        em.persist(tag);

        em.getTransaction().commit();

        em.close();
        System.out.println("Tag was created. id:" + tag.getId());
        return tag;
    }

    @Override
    public Tag update(final Tag tag) {
        EntityManager em = EntityManagerUtils.getEntityManager();

        em.getTransaction().begin();

        em.persist(tag);

        em.getTransaction().commit();

        em.close();
        System.out.println("Tag was updated. id:" + tag.getId());
        return tag;
    }

    @Override
    public void delete(final Long id) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        Tag toDelete = em.find(Tag.class,id);
        em.remove(toDelete);

        em.getTransaction().commit();
        em.close();
        System.out.println("Tag was deleted. id:" + toDelete.getId());

    }
}
