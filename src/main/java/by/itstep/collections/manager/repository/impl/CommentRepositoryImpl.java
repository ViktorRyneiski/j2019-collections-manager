package by.itstep.collections.manager.repository.impl;

import by.itstep.collections.manager.entity.Collection;
import by.itstep.collections.manager.entity.Comment;
import by.itstep.collections.manager.repository.CommentRepository;
import by.itstep.collections.manager.util.EntityManagerUtils;

import java.util.List;
import javax.persistence.EntityManager;

public class CommentRepositoryImpl implements CommentRepository {

    @Override
    public List<Comment> findAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();

        List<Comment> foundComments = em.createNativeQuery("SELECT * FROM comments",Comment.class)
                .getResultList();

        em.close();

        System.out.println("Found: " + foundComments.size() + " comments");
        return foundComments;
    }

    @Override
    public Comment findById(final Long id) {
        EntityManager em = EntityManagerUtils.getEntityManager();

        Comment foundComment = em.find(Comment.class,id);

        em.close();

        System.out.println("Found comment: " + foundComment);
        return foundComment;
    }

    @Override
    public Comment create(final Comment comment) {
        EntityManager em = EntityManagerUtils.getEntityManager();

        em.getTransaction().begin();

        em.persist(comment);

        em.getTransaction().commit();

        em.close();
        System.out.println("Comment was created. id:" + comment.getId());
        return null;
    }

    @Override
    public Comment update(final Comment comment) {
        EntityManager em = EntityManagerUtils.getEntityManager();

        em.getTransaction().begin();

        em.merge(comment);

        em.getTransaction().commit();

        em.close();
        System.out.println("Comment was updated. id:" + comment.getId());
        return comment;
    }

    @Override
    public void delete(final Long id) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        Comment toDelete = em.find(Comment.class,id);
        em.remove(toDelete);

        em.getTransaction().commit();
        em.close();
        System.out.println("Comment was deleted. id:" + toDelete.getId());

    }
}
