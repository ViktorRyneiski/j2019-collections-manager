package by.itstep.collections.manager.repository.impl;

import by.itstep.collections.manager.entity.CollectionItem;
import by.itstep.collections.manager.repository.CollectionItemRepository;
import by.itstep.collections.manager.util.EntityManagerUtils;

import java.util.List;
import javax.persistence.EntityManager;

public class CollectionItemRepositoryImpl implements CollectionItemRepository {

    @Override
    public List<CollectionItem> findAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();

        List<CollectionItem> foundCollectionItems = em.createNativeQuery("SELECT * FROM collection_items",CollectionItem.class)
                .getResultList();
        em.close();
        System.out.println("Found: " + foundCollectionItems.size() + " collectionItems");
        return foundCollectionItems;
    }

    @Override
    public CollectionItem findById(final Long id) {
        EntityManager em = EntityManagerUtils.getEntityManager();

        CollectionItem foundCollectionItem = em.find(CollectionItem.class,id);

        em.close();
        System.out.println("Found collectionItem: " + foundCollectionItem);
        return foundCollectionItem;
    }

    @Override
    public CollectionItem create(final CollectionItem collectionItem) {
        EntityManager em = EntityManagerUtils.getEntityManager();

        em.getTransaction().begin();

        em.persist(collectionItem);

        em.getTransaction().commit();

        em.close();
        System.out.println("CollectionItem was created. id:" + collectionItem.getId());
        return collectionItem;
    }

    @Override
    public CollectionItem update(final CollectionItem collectionItem) {
        EntityManager em = EntityManagerUtils.getEntityManager();

        em.getTransaction().begin();

        em.merge(collectionItem);

        em.getTransaction().commit();

        em.close();
        System.out.println("CollectionItem was updated. id:" + collectionItem.getId());

        return null;
    }

    @Override
    public void delete(final Long id) {
        EntityManager em = EntityManagerUtils.getEntityManager();

        em.getTransaction().begin();

        CollectionItem toDelete = em.find(CollectionItem.class,id);

        em.remove(toDelete);

        em.getTransaction().commit();

        em.close();
        System.out.println("CollectionItem was deleted. id:" + toDelete.getId());
    }

    @Override
    public void deleteAll() {

        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.createNativeQuery("DELETE FROM collection_items").executeUpdate();

        em.getTransaction().commit();
        em.close();
    }
}
